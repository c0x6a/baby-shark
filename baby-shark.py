sharks = [
    "Baby", "Daddy", "Mommy", "Grandma", "Grandpa",
]
extra_words = [
    "Let's go hunt", "Run away", "Safe at last", "It's the end",
]

song_part1 = "\n".join(
    ["".join(
        [f"{shark} shark,{' doo' * 6}.\n" * 3, f"{shark} shark!\n"]
    ) for shark in sharks]
)

song_part2 = "\n".join(
    ["".join(
        [f"{word},{' doo' * 6}.\n" * 3, f"{word}!\n"]
    ) for word in extra_words]
)

print(f"{song_part1}\n{song_part2}")
